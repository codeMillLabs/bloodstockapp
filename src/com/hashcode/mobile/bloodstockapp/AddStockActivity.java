package com.hashcode.mobile.bloodstockapp;

import static com.hashcode.mobile.bloodstockapp.AddStockListActivity.bloodstocks;
import static com.hashcode.mobile.bloodstockapp.SettingsActivity.SHARED_PREF_NAME;
import static com.hashcode.mobile.bloodstockapp.util.HttpUtil.isConnected;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hashcode.mobile.bloodstockapp.model.BloodStockModel;

public class AddStockActivity extends Activity
{

    private DatePicker datePicker;
    private Spinner bloodTypeSpinner;
    private Spinner subTypeSpinner;
    private Spinner statusTypeSpinner;
    private EditText stockInfo;
    private Button saveButton;
    private Button cancelButton;
    private TextView txtView;
    private BloodStockModel bloodStockData;

    private SharedPreferences settings;
    protected static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stock);

        datePicker = (DatePicker) findViewById(R.id.dateField);
        bloodTypeSpinner = (Spinner) findViewById(R.id.bloodTypeField);
        statusTypeSpinner = (Spinner) findViewById(R.id.statusTypeField);
        subTypeSpinner = (Spinner) findViewById(R.id.subTypeField);
        subTypeSpinner.setOnItemSelectedListener(onSubTypeChangeListener());

        stockInfo = (EditText) findViewById(R.id.stockField);
        txtView = (TextView) findViewById(R.id.appIsConnected);

        saveButton = (Button) findViewById(R.id.addStockButton);
        saveButton.setOnClickListener(saveStockListener());

        cancelButton = (Button) findViewById(R.id.cancelStockButton);
        cancelButton.setOnClickListener(cancelStockButtonListener());

        settings = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);

        // check if you are connected or not
        if (isConnected(this))
        {
            txtView.setBackgroundColor(0xFF00CC00);
            txtView.setText("You are conncted");
        }
        else
        {
            txtView.setText("You are NOT conncted");
        }
    }

    private OnItemSelectedListener onSubTypeChangeListener()
    {
        return new OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                String subType = (String) adapterView.getItemAtPosition(pos);
                Log.d("BBA", "Selected sub type [" + subType + "]");

                if (subType.equals("RCC"))
                {
                    statusTypeSpinner.setEnabled(true);
                    String[] rbcStatusTypes = getResources().getStringArray(R.array.rbc_status_type_array);
                    ArrayAdapter<String> dataAdapter =
                        new ArrayAdapter<String>(AddStockActivity.this, android.R.layout.simple_spinner_item,
                            rbcStatusTypes);
                    statusTypeSpinner.setAdapter(dataAdapter);
                }
                else
                {
                    String[] otherStatusTypes = new String[]{};
                    ArrayAdapter<String> dataAdapter =
                        new ArrayAdapter<String>(AddStockActivity.this, android.R.layout.simple_spinner_item,
                            otherStatusTypes);
                    statusTypeSpinner.setAdapter(dataAdapter);
                    statusTypeSpinner.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }

        };
    }

    private boolean validate()
    {
        if (stockInfo.getText().toString().trim().equals(""))
            return false;
        else
            return true;
    }

    private boolean isDataConnected()
    {
        return isConnected(this);
    }

    private OnClickListener saveStockListener()
    {

        OnClickListener clickListner = new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                if (!isDataConnected())
                {
                    Toast.makeText(getApplicationContext(), R.string.notConnected_Key, Toast.LENGTH_LONG).show();
                    return;
                }

                if (!validate())
                {
                    Toast.makeText(getApplicationContext(), R.string.fill_required_fields_Key, Toast.LENGTH_LONG)
                        .show();
                    return;
                }

                String centerId = settings.getString(getString(R.string.centerIdKey), "empty");
                String date = dateFormat.format(new Date(datePicker.getCalendarView().getDate()));
                String bloodType = bloodTypeSpinner.getSelectedItem().toString();
                String subType = subTypeSpinner.getSelectedItem().toString();
                String statusType = (statusTypeSpinner.getSelectedItem() != null) ? statusTypeSpinner.getSelectedItem().toString() : null;
                String stockCount = stockInfo.getText().toString();

                bloodStockData = new BloodStockModel();
                bloodStockData.setDate(date);
                bloodStockData.setCenterId(centerId);
                bloodStockData.setBloodType(bloodType);
                bloodStockData.setSubType(subType);
                bloodStockData.setStatusSubType(statusType);
                bloodStockData.setStockCount(stockCount);

                bloodstocks.add(bloodStockData);

                Intent intent = new Intent(getApplicationContext(), AddStockListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        };

        return clickListner;
    }

    private OnClickListener cancelStockButtonListener()
    {
        OnClickListener clickListner = new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), AddStockListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        };
        return clickListner;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.add_stock, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
