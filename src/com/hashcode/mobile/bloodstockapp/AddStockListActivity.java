package com.hashcode.mobile.bloodstockapp;

import static com.hashcode.mobile.bloodstockapp.SettingsActivity.SHARED_PREF_NAME;
import static com.hashcode.mobile.bloodstockapp.util.HttpUtil.isConnected;
import static com.hashcode.mobile.bloodstockapp.util.HttpUtil.sendPOSTRequest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.hashcode.mobile.bloodstockapp.model.BloodStockModel;

public class AddStockListActivity extends Activity
{

    private Button addButton;
    private Button saveAllButton;
    private Button cancelButton;
    private ProgressDialog progress;
    private TableLayout newStockTable;

    private LayoutParams tableLayoutParam = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

    protected static List<BloodStockModel> bloodstocks = new ArrayList<BloodStockModel>();
    private boolean savingFailed = false;

    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stock_list);

        addButton = (Button) findViewById(R.id.addNewStockButton);
        addButton.setOnClickListener(stockButtonListener(R.id.addNewStockButton));

        saveAllButton = (Button) findViewById(R.id.saveAllStockButton);
        saveAllButton.setOnClickListener(stockButtonListener(R.id.saveAllStockButton));

        cancelButton = (Button) findViewById(R.id.cancelAllStockSaveButton);
        cancelButton.setOnClickListener(stockButtonListener(R.id.cancelAllStockSaveButton));

        newStockTable = (TableLayout) findViewById(R.id.addStockDataTable);

        settings = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);

        TableRow header = createTableHeader();
        newStockTable.addView(header,
            new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        Log.i("BBA - New Stocks Size", "" + bloodstocks.size());
        for (int i = 0; i < bloodstocks.size(); i++)
        {
            BloodStockModel stock = bloodstocks.get(i);
            newStockTable.addView(
                createTableRow(i, stock.getDate(), stock.getBloodType(), stock.getSubType(), stock.getStatusSubType(),
                    stock.getStockCount()), tableLayoutParam);
        }

        progress = new ProgressDialog(this);
        progress.setTitle("Data Sending");
        progress.setMessage("Wait while data sending ...");

    }

    private TableRow createTableRow(final int rowCount, String date, String bloodType, String subType,
        String statusSubType, String count)
    {

        TableRow tr = new TableRow(this);
        tr.setWeightSum(1);

        if (rowCount % 2 == 0)
        {
            tr.setBackgroundColor(Color.parseColor(getString(R.color.odd_row_color)));
        }
        else
        {
            tr.setBackgroundColor(Color.TRANSPARENT);
        }
        tr.setLayoutParams(tableLayoutParam);
        tr.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView dateRow = new TextView(this);
        dateRow.setText(date);
        dateRow.setTextColor(Color.BLACK);
        dateRow.setGravity(Gravity.CENTER);
        tr.addView(dateRow);

        TextView bloodTypeRow = new TextView(this);
        bloodTypeRow.setText(bloodType);
        bloodTypeRow.setTextColor(Color.BLACK);
        bloodTypeRow.setGravity(Gravity.CENTER);
        tr.addView(bloodTypeRow);

        TextView subTypeRow = new TextView(this);
        subType =
            subType
                + ((statusSubType != null && !statusSubType.isEmpty()) ? (statusSubType.startsWith("Cross")) ? "(C)"
                    : "(U)" : "");
        subTypeRow.setText(subType);
        subTypeRow.setTextColor(Color.BLACK);
        subTypeRow.setGravity(Gravity.CENTER);

        tr.addView(subTypeRow);

        TextView stockCountRow = new TextView(this);
        stockCountRow.setText(count);
        stockCountRow.setTextColor(Color.BLACK);
        stockCountRow.setGravity(Gravity.CENTER);
        tr.addView(stockCountRow);

        // Delete  button
        final Button button = new Button(this);
        button.setText("Delete");
        button.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
            TableRow.LayoutParams.WRAP_CONTENT));
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final TableRow parent = (TableRow) v.getParent();
                bloodstocks.remove(rowCount);
                newStockTable.removeView(parent);
            }
        });
        tr.addView(button);

        return tr;
    }

    private TableRow createTableHeader()
    {
        TableRow trHead = new TableRow(this);
        trHead.setWeightSum(1);
        trHead.setBackgroundColor(Color.parseColor(getString(R.color.button_overlay)));
        trHead.setLayoutParams(tableLayoutParam);
        trHead.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView date = new TextView(this);
        date.setText("DATE");
        date.setTextColor(Color.WHITE);
        date.setGravity(Gravity.CENTER);
        trHead.addView(date);

        TextView bloodType = new TextView(this);
        bloodType.setText("BLOOD TYPE");
        bloodType.setTextColor(Color.WHITE);
        bloodType.setGravity(Gravity.CENTER);
        trHead.addView(bloodType);

        TextView subType = new TextView(this);
        subType.setText("SUB TYPE");
        subType.setTextColor(Color.WHITE);
        subType.setGravity(Gravity.CENTER);
        trHead.addView(subType);

        TextView stockCountType = new TextView(this);
        stockCountType.setText("STOCK COUNT");
        stockCountType.setTextColor(Color.WHITE);
        stockCountType.setGravity(Gravity.LEFT);
        trHead.addView(stockCountType);

        TextView action = new TextView(this);
        action.setText("ACTION");
        action.setTextColor(Color.WHITE);
        action.setGravity(Gravity.CENTER);
        trHead.addView(action);

        return trHead;
    }

    private OnClickListener stockButtonListener(final int id)
    {

        OnClickListener clickListner = new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                Intent intent = null;
                switch (id)
                {
                    case R.id.addNewStockButton:
                    {
                        intent = new Intent(getApplicationContext(), AddStockActivity.class);
                        break;
                    }
                    case R.id.saveAllStockButton:
                    {
                        saveStocks();
                        if (savingFailed)
                            return;

                        intent = new Intent(getApplicationContext(), MainAppActivity.class);
                        break;
                    }
                    case R.id.cancelAllStockSaveButton:
                    {
                        intent = new Intent(getApplicationContext(), MainAppActivity.class);
                        clearStocks();
                        break;
                    }
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

        };
        return clickListner;
    }

    private void saveStocks()
    {
        if (!isConnected(this))
        {
            Toast.makeText(getApplicationContext(), R.string.notConnected_Key, Toast.LENGTH_LONG).show();
            savingFailed = true;
            return;
        }
        new HttpAsyncTask().execute(getString(R.string.add_blood_stock_url_Key));

    }

    public static void clearStocks()
    {
        bloodstocks.clear();
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... urls)
        {
            try
            {
                savingFailed = false;
                JSONArray bloodStocksJSON = new JSONArray();
                JSONObject stocksJSON = new JSONObject();
                String centerId = settings.getString(getString(R.string.centerIdKey), "empty");
                stocksJSON.put("centerId", centerId);
                stocksJSON.put("size", bloodstocks.size());
                stocksJSON.put("bloodStocks", bloodStocksJSON);

                Log.d("BBA - size", "" + bloodstocks.size());
                for (BloodStockModel bloodStock : bloodstocks)
                {
                    JSONObject stockJSON = new JSONObject();
                    stockJSON.accumulate("date", bloodStock.getDate());
                    stockJSON.accumulate("centerId", bloodStock.getCenterId());
                    stockJSON.accumulate("bloodType", bloodStock.getBloodType());
                    stockJSON.accumulate("subType", bloodStock.getSubType());
                    stockJSON.accumulate("statusSubType", bloodStock.getStatusSubType());
                    stockJSON.accumulate("stockCount", bloodStock.getStockCount());
                    bloodStocksJSON.put(stockJSON);
                }
                Log.d("BBA - New Stocks Data", stocksJSON.toString());
                return sendPOSTRequest(urls[0], stocksJSON);
            }
            catch (Exception e)
            {
                Log.e("BBA-Stock Saving failed", e.getLocalizedMessage());
                savingFailed = true;
                return null;
            }
            finally
            {
                clearStocks();
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result)
        {
            try
            {
                JSONObject jSon = new JSONObject(result);
                //String status = jSon.getString("status");
                String message = jSon.getString("message");

                Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
            catch (Exception e)
            {
                Log.e("BBA", "Error occurred in saving blood stocks, " + e.getLocalizedMessage());
                savingFailed = true;
            }
        }
    }

}
