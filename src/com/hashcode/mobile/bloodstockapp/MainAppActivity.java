package com.hashcode.mobile.bloodstockapp;

import static com.hashcode.mobile.bloodstockapp.SettingsActivity.SHARED_PREF_NAME;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hashcode.mobile.bloodstockapp.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and navigation/system bar) with
 * user interaction.
 * 
 * @see SystemUiHider
 */
public class MainAppActivity extends Activity
{
    /**
     * Whether or not the system UI should be auto-hidden after {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after user interaction before hiding the system
     * UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise, will show the system UI visibility upon
     * interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    private Button addStockButton;
    private Button viewStockButton;
    private Button settingsButton;
    private SharedPreferences settings;
    private String centerId;

    private Dialog loginDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_app);

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener()
        {
            // Cached values.
            int mControlsHeight;
            int mShortAnimTime;

            @Override
            @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
            public void onVisibilityChange(boolean visible)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
                {
                    // If the ViewPropertyAnimator API is available
                    // (Honeycomb MR2 and later), use it to animate the
                    // in-layout UI controls at the bottom of the
                    // screen.
                    if (mControlsHeight == 0)
                    {
                        mControlsHeight = controlsView.getHeight();
                    }
                    if (mShortAnimTime == 0)
                    {
                        mShortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                    }
                    controlsView.animate().translationY(visible ? 0 : mControlsHeight).setDuration(mShortAnimTime);
                }
                else
                {
                    // If the ViewPropertyAnimator APIs aren't
                    // available, simply show or hide the in-layout UI
                    // controls.
                    controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                }

                if (visible && AUTO_HIDE)
                {
                    // Schedule a hide().
                    delayedHide(AUTO_HIDE_DELAY_MILLIS);
                }
            }
        });

        settings = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (TOGGLE_ON_CLICK)
                {
                    mSystemUiHider.toggle();
                }
                else
                {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.

        centerId = settings.getString(getString(R.string.centerIdKey), "empty");

        addStockButton = (Button) findViewById(R.id.addBloodStockButton);
        addStockButton.setOnClickListener(addStockButtonListener());

        viewStockButton = (Button) findViewById(R.id.viewBloodStockButton);
        viewStockButton.setOnClickListener(viewStockButtonListener());

        settingsButton = (Button) findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(settingsButtonListener());

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private OnClickListener addStockButtonListener()
    {

        OnClickListener clickListner = new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d("BBA Main ", "Center Id " + centerId);
                if (centerId == null || centerId.isEmpty() || "empty".equals(centerId))
                {
                    popupLoginDialog("SETTINGS");
                }
                else
                {
                    Intent intent = new Intent(getApplicationContext(), AddStockListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        };

        return clickListner;
    }

    private OnClickListener viewStockButtonListener()
    {

        OnClickListener clickListner = new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d("BBA Main ", "Center Id " + centerId);
                if (centerId == null || centerId.isEmpty() || "empty".equals(centerId))
                {
                    popupLoginDialog("SETTINGS");
                }
                else
                {
                    Intent intent = new Intent(getApplicationContext(), ViewStockActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        };

        return clickListner;
    }

    private OnClickListener settingsButtonListener()
    {

        OnClickListener clickListner = new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                popupLoginDialog("SETTINGS");
            }
        };

        return clickListner;
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the system UI. This is to prevent the jarring
     * behavior of controls going away while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            if (AUTO_HIDE)
            {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any previously scheduled calls.
     */
    private void delayedHide(int delayMillis)
    {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private void popupLoginDialog(final String viewId)
    {
        loginDialog = new Dialog(this);
        loginDialog.setContentView(R.layout.login_dialog);
        loginDialog.setTitle("Login");

        final TextView userNameText = (TextView) loginDialog.findViewById(R.id.userNameField);
        final TextView passwordText = (TextView) loginDialog.findViewById(R.id.passwordField);

        Button btnLogin = (Button) loginDialog.findViewById(R.id.loginButton);
        Button btnCancel = (Button) loginDialog.findViewById(R.id.cancelButton);

        btnLogin.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String userName = userNameText.getText().toString();
                String password = passwordText.getText().toString();

                if (userName.equals("admin") && password.equals("@admin"))
                {
                    Intent intent = null;
                    if (viewId.equals("ADD"))
                    {
                        intent = new Intent(getApplicationContext(), AddStockListActivity.class);
                    }
                    else if (viewId.equals("VIEW"))
                    {
                        intent = new Intent(getApplicationContext(), ViewStockActivity.class);
                    }
                    else
                    {
                        intent = new Intent(getApplicationContext(), SettingsActivity.class);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), R.string.invalid_user_credentials_key, Toast.LENGTH_LONG)
                        .show();
                }
            }
        });

        btnCancel.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loginDialog.dismiss();
            }
        });

        loginDialog.show();
    }
}
