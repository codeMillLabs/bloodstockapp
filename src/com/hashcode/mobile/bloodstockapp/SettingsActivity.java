package com.hashcode.mobile.bloodstockapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends Activity {

	public static final String SHARED_PREF_NAME = "BLOOD_STOCK";

	private EditText centerIdField;
	private Button saveButton;
	private Button cancelButton;

	private SharedPreferences settings;
	private SharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		centerIdField = (EditText) findViewById(R.id.centerIdField);
		saveButton = (Button) findViewById(R.id.saveSettingsButton);
		saveButton.setOnClickListener(saveButtonClickListener());

		cancelButton = (Button) findViewById(R.id.cancelSettingsButton);
		cancelButton.setOnClickListener(cancelButtonListener());

		settings = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
		editor = settings.edit();

		centerIdField.setText(settings.getString(
				getString(R.string.centerIdKey), ""));
	}

	private OnClickListener saveButtonClickListener() {

		OnClickListener clickListner = new OnClickListener() {

			@Override
			public void onClick(View v) {

				String centerId = centerIdField.getText().toString();

				editor.putString(getString(R.string.centerIdKey), centerId);
				editor.commit();
				
				Toast.makeText(getApplicationContext(), "Center Id saved.",
						1000).show();
				Intent intent = new Intent(getApplicationContext(),
						MainAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
			}
		};

		return clickListner;
	}

	private OnClickListener cancelButtonListener() {

		OnClickListener clickListner = new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						MainAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
			}
		};
		return clickListner;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
