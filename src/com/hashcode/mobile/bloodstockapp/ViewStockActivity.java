package com.hashcode.mobile.bloodstockapp;

import static android.widget.Toast.LENGTH_LONG;
import static com.hashcode.mobile.bloodstockapp.AddStockActivity.dateFormat;
import static com.hashcode.mobile.bloodstockapp.SettingsActivity.SHARED_PREF_NAME;
import static com.hashcode.mobile.bloodstockapp.util.HttpUtil.ERROR_STATUS_CODE;
import static com.hashcode.mobile.bloodstockapp.util.HttpUtil.INVALID_CENTER_STATUS_CODE;
import static com.hashcode.mobile.bloodstockapp.util.HttpUtil.NO_DATA_STATUS_CODE;
import static com.hashcode.mobile.bloodstockapp.util.HttpUtil.sendPOSTRequest;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ViewStockActivity extends Activity
{

    private TableLayout viewTable;
    private Button backButton;
    private ProgressDialog progress;
    private Dialog searchDialog;
    private DatePicker datePicker;
    private Spinner centerSpinner;
    private Button searchButton;

    private SharedPreferences settings;

    private LayoutParams tableLayoutParam = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_stock);

        settings = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);

        backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(backButtonListener());
        viewTable = (TableLayout) findViewById(R.id.viewDataTable);
        TableRow trHead = createHeaderRow();

        progress = new ProgressDialog(this);
        progress.setTitle("Data Loading");
        progress.setMessage("Wait while data loading ...");
        
        searchDialog = new Dialog(this);
        searchDialog.setContentView(R.layout.search_dialog);
        searchDialog.setTitle("Search");
        
        datePicker = (DatePicker) searchDialog.findViewById(R.id.searchDateField);
        centerSpinner = (Spinner) searchDialog.findViewById(R.id.searchCenterField);
        searchButton = (Button) searchDialog.findViewById(R.id.searchButton);
       
        String currentDate = dateFormat.format(Calendar.getInstance().getTime());
        String centerId = settings.getString(getString(R.string.centerIdKey), "empty");

        StockDataAsyncTask dataRequestTask = new StockDataAsyncTask(currentDate, centerId);
        dataRequestTask.execute(getString(R.string.view_blood_stock_url_Key));

        CenterDataAsyncTask centerDataLoadTask = new CenterDataAsyncTask();
        centerDataLoadTask.execute(getString(R.string.center_id_url_Key));

        progress.show();

        viewTable.addView(trHead, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    private TableRow createHeaderRow()
    {
        TableRow trHead = new TableRow(this);
        trHead.setWeightSum(1);
        trHead.setBackgroundColor(Color.parseColor(getString(R.color.button_overlay)));
        trHead.setLayoutParams(tableLayoutParam);
        trHead.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView date = new TextView(this);
        date.setText("DATE");
        date.setTextColor(Color.WHITE);
        date.setGravity(Gravity.CENTER);
        trHead.addView(date);

        TextView bloodType = new TextView(this);
        bloodType.setText("BLOOD TYPE");
        bloodType.setTextColor(Color.WHITE);
        bloodType.setGravity(Gravity.CENTER);
        trHead.addView(bloodType);

        TextView subType = new TextView(this);
        subType.setText("SUB TYPE");
        subType.setTextColor(Color.WHITE);
        subType.setGravity(Gravity.CENTER);
        trHead.addView(subType);

        TextView stockCountType = new TextView(this);
        stockCountType.setText("STOCK COUNT");
        stockCountType.setTextColor(Color.WHITE);
        stockCountType.setGravity(Gravity.RIGHT);
        trHead.addView(stockCountType);
        return trHead;
    }

    private class StockDataAsyncTask extends AsyncTask<String, Void, String>
    {
        String centerId;
        String date;

        public StockDataAsyncTask(String date, String centerId)
        {
            this.centerId = centerId;
            this.date = date;
        }

        @Override
        protected String doInBackground(String... urls)
        {
            Map<String, String> dataMap = new HashMap<String, String>();
            dataMap.put("date", date);
            dataMap.put("centerId", centerId);

            return sendPOSTRequest(urls[0], dataMap);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result)
        {

            // Toast.makeText(getApplicationContext(), result, 2000).show();
            try
            {
                JSONObject jSon = new JSONObject(result);
                String status = jSon.getString("status");
                String message = jSon.getString("message");

                if (ERROR_STATUS_CODE.equals(status) || INVALID_CENTER_STATUS_CODE.equals(status))
                {
                    Toast.makeText(getApplicationContext(), message, LENGTH_LONG).show();
                }
                else if (NO_DATA_STATUS_CODE.equals(status))
                {
                    Toast.makeText(getApplicationContext(), message, LENGTH_LONG).show();
                }
                else
                {
                    JSONArray dataArray = jSon.getJSONArray("bloodStocks");
                    for (int i = 0; i < dataArray.length(); i++)
                    {
                        JSONObject row = dataArray.getJSONObject(i);
                        viewTable.addView(
                            createTableRow(i, row.getString("date"), row.getString("bloodType"),
                                row.getString("subType"), row.getString("statusSubType"), row.getString("stockCount")),
                            tableLayoutParam);
                    }
                }
            }
            catch (Exception e)
            {
                Log.e("BBA", "Error occurred in view blood stock view, " + e.getLocalizedMessage());
            }
            progress.dismiss();
        }
    }

    private class CenterDataAsyncTask extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... urls)
        {
            Map<String, String> dataMap = new HashMap<String, String>();
            return sendPOSTRequest(urls[0], dataMap);
        }

        @Override
        protected void onPostExecute(String result)
        {
            //Toast.makeText(getApplicationContext(), result, 2000).show();
            try
            {
                JSONArray dataArray = new JSONArray(result);
                String[] centerDetails = new String[dataArray.length()];
                for (int i = 0; i < dataArray.length(); i++)
                {
                    JSONObject val = dataArray.getJSONObject(i);
                    centerDetails[i] = val.getString("centerId");
                }
                
                ArrayAdapter<String> dataAdapter =
                    new ArrayAdapter<String>(ViewStockActivity.this, android.R.layout.simple_spinner_item,
                        centerDetails);
                centerSpinner.setAdapter(dataAdapter);
            }
            catch (Exception e)
            {
                Log.e("BBA", "Error occurred in loading center data to search form, " + e.getLocalizedMessage());
            }
            progress.dismiss();
        }
    }

    private TableRow createTableRow(int rowCount, String date, String bloodType, String subType, String statusSubType,
        String count)
    {
        TableRow tr = new TableRow(this);
        tr.setWeightSum(1);

        if (rowCount % 2 == 0)
        {
            tr.setBackgroundColor(Color.parseColor(getString(R.color.odd_row_color)));
        }
        else
        {
            tr.setBackgroundColor(Color.TRANSPARENT);
        }

        tr.setLayoutParams(tableLayoutParam);
        tr.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView dateRow = new TextView(this);
        dateRow.setText(date);
        dateRow.setTextColor(Color.BLACK);
        dateRow.setGravity(Gravity.CENTER);
        tr.addView(dateRow);

        TextView bloodTypeRow = new TextView(this);
        bloodTypeRow.setText(bloodType);
        bloodTypeRow.setTextColor(Color.BLACK);
        bloodTypeRow.setGravity(Gravity.CENTER);
        tr.addView(bloodTypeRow);

        TextView subTypeRow = new TextView(this);
        subType =
            subType
                + ((statusSubType != null && !statusSubType.isEmpty()) ? (statusSubType.startsWith("Cross")) ? "(C)"
                    : "(U)" : "");
        subTypeRow.setText(subType);
        subTypeRow.setTextColor(Color.BLACK);
        subTypeRow.setGravity(Gravity.CENTER);
        tr.addView(subTypeRow);

        TextView stockCountRow = new TextView(this);
        stockCountRow.setText(count);
        stockCountRow.setTextColor(Color.BLACK);
        stockCountRow.setGravity(Gravity.CENTER);
        tr.addView(stockCountRow);

        return tr;
    }

    private OnClickListener backButtonListener()
    {

        OnClickListener clickListner = new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), MainAppActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        };
        return clickListner;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.view_stock, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch (id)
        {
            case R.id.stock_search_menu_item:
                searchDialog();
                return true;

            case R.id.about_us_menu_item:

                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void searchDialog()
    {
        searchButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                viewTable.removeAllViews();

                String date = dateFormat.format(new Date(datePicker.getCalendarView().getDate()));
                String centerId =
                    (centerSpinner.getSelectedItem() != null) ? centerSpinner.getSelectedItem().toString() : settings
                        .getString(getString(R.string.centerIdKey), "");

                StockDataAsyncTask dataRequestTask = new StockDataAsyncTask(date, centerId);
                dataRequestTask.execute(getString(R.string.view_blood_stock_url_Key));
                progress.show();
                viewTable.addView(createHeaderRow(), new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT));
                searchDialog.dismiss();
            }
        });
        searchDialog.show();
        searchDialog.getWindow().setLayout(1100, 1350);
    }

}
