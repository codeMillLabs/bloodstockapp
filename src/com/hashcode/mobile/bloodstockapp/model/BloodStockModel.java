/**
 * 
 */
package com.hashcode.mobile.bloodstockapp.model;

import java.util.HashMap;
import java.util.Map;


/**
 * <p>
 * Blood stock data model
 * </p>
 * 
 * @author Amila Silva
 * 
 */
public class BloodStockModel {

	private String date;
	private String bloodType;
	private String subType;
	private String statusSubType;
	private String centerId;
	private String stockCount;
	
	private Map<String, String> dataMap = new HashMap<String, String>();

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the bloodType
	 */
	public String getBloodType() {
		return bloodType;
	}

	/**
	 * @param bloodType
	 *            the bloodType to set
	 */
	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType
	 *            the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the centerId
	 */
	public String getCenterId() {
		return centerId;
	}

	/**
	 * @param centerId
	 *            the centerId to set
	 */
	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	/**
	 * @return the stockCount
	 */
	public String getStockCount() {
		return stockCount;
	}

	/**
	 * @param stockCount
	 *            the stockCount to set
	 */
	public void setStockCount(String stockCount) {
		this.stockCount = stockCount;
	}

	/**
     * <p>
     * Getter for statusSubType.
     * </p>
     * 
     * @return the statusSubType
     */
    public String getStatusSubType()
    {
        return statusSubType;
    }

    /**
     * <p>
     * Setting value for statusSubType.
     * </p>
     * 
     * @param statusSubType the statusSubType to set
     */
    public void setStatusSubType(String statusSubType)
    {
        this.statusSubType = statusSubType;
    }

    /**
	 * @return the dataMap
	 */
	public Map<String, String> getDataMap() {
		dataMap.put("date", getDate());
		dataMap.put("centerId", getCenterId());
		dataMap.put("bloodType", getBloodType());
		dataMap.put("subType", getSubType());
		dataMap.put("statusSubtype", getStatusSubType());
		dataMap.put("stock", getStockCount());
		
		return dataMap;
	}

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String
            .format(
                "BloodStockModel [date=%s, bloodType=%s, subType=%s, statusSubType=%s, centerId=%s, stockCount=%s, dataMap=%s]",
                date, bloodType, subType, statusSubType, centerId, stockCount, dataMap);
    }

}
