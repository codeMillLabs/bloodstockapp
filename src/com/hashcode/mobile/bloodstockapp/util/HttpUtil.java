/**
 * 
 */
package com.hashcode.mobile.bloodstockapp.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * @author Amila Silva
 * 
 */
public class HttpUtil
{

    public static final String SUCCESS_STATUS_CODE = "S1000";
    public static final String ERROR_STATUS_CODE = "S5000";
    public static final String NO_DATA_STATUS_CODE = "S4012";
    public static final String INVALID_CENTER_STATUS_CODE = "S4014";

    /**
     * <p>
     * Utility to check connection is available.
     * </p>
     * 
     * @param activity
     *            activity
     * @return true or false on availability of the connection
     */
    public static boolean isConnected(Activity activity)
    {
        ConnectivityManager connMgr = (ConnectivityManager) activity.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    /**
     * <p>
     * Send the POST request to server and receive response in json format.
     * </p>
     * 
     * @param url
     *            URL of the server
     * @param dataMap
     *            data to be send
     * @return json response
     * @throws JSONException
     */
    public static String sendPOSTRequest(String url, Map<String, String> dataMap)
    {
        try
        {

            JSONObject jsonObject = new JSONObject();
            for (Entry<String, String> entry : dataMap.entrySet())
            {
                jsonObject.put(entry.getKey(), entry.getValue());
            }

            return sendPOSTRequest(url, jsonObject);
        }
        catch (Exception e)
        {
            Log.e("BBA-Send Request Failed", e.getLocalizedMessage());
        }
        return "";
    }

    /**
     * <p>
     * Send the POST request to server and receive response in json format.
     * </p>
     * 
     * @param url
     *            URL of the server
     * @param jsonObj
     *            JSONObject to be send
     * @return json response
     * 
     */
    public static String sendPOSTRequest(String url, JSONObject jsonObj)
    {
        InputStream inputStream = null;
        String result = "";
        try
        {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = jsonObj.toString();

            StringEntity se = new StringEntity(json);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setEntity(se);

            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = failedResponse();
            Log.i("BBA-Send Request", "Request successfully sent");
        }
        catch (Exception e)
        {
            Log.e("BBA-Send Request Failed", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException
    {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private static String failedResponse()
    {
        try
        {
            JSONObject json = new JSONObject();
            json.put("status", "500");
            json.put("message", "Failed Request");

            return json.toString();
        }
        catch (Exception e)
        {
            Log.e("BBA", "Failed to create failed response, " + e.getLocalizedMessage());
        }
        return "";
    }

}
